import React from "react";
import Layout from "../../lib/components/layout";
import {Container, SvgIcon} from "@mui/material";
import {routes} from "../../router/routes";
import {Link} from "react-router-dom";
import {isCurrentRoute} from "../../lib/helpers";

const welcomeText = "Домашняя страница, где каждый самурай должен чувствовать себя как дома";

const HomePage = () => {
	return (
		<Layout>
			<div className="home-page">
				<div className="home-page__content">
					<div className="home-page__welcome-text">
						{ welcomeText }
					</div>
					
					<div className="home-page__routes">
						{ routes.filter(route => !isCurrentRoute(route.url)).map(route => (
							<Link className="home-page__route" to={route.url}>
								<SvgIcon component={route.icon} />
								<span className="home-page__route-title">
									{ route.title }
								</span>
							</Link>
						)) }
					</div>
				</div>
			</div>
		</Layout>
	)
};

export default HomePage;