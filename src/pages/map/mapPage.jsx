import React, {useEffect} from "react";
import MapGrid from "./components/mapGrid";
import Layout from "../../lib/components/layout";
import useKeyActions from "./hooks/useKeyActions";
import ControlPanel from "./components/controlPanel";

const MapPage = () => {
	const { keyDownCallbackWrapper, keyUpCallbackWrapper, keyPressCallbackWrapper } = useKeyActions();
	
	useEffect(() => {
		window.addEventListener("keydown", keyDownCallbackWrapper);
		window.addEventListener("keyup", keyUpCallbackWrapper);
		window.addEventListener("keypress", keyPressCallbackWrapper);
		
		return () => {
			window.removeEventListener("keydown", keyDownCallbackWrapper);
			window.removeEventListener("keyup", keyUpCallbackWrapper);
			window.removeEventListener("keypress", keyPressCallbackWrapper);
		}
	}, [])
	
	return (
		<Layout>
			<div className="map-page">
				<MapGrid />
				<ControlPanel />
			</div>
		</Layout>
	);
};

export default MapPage;
