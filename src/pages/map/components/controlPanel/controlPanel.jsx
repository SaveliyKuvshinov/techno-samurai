import React, {useCallback, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import classNames from "classnames";
import {changeGridCellSize, toggleControlPanel} from "../../../../store/map/mapSlice";
import SettingsIcon from '@mui/icons-material/Settings';
import CloseIcon from '@mui/icons-material/Close';
import {Divider, Slider} from "@mui/material";
import {useDebounce} from "../../../../lib/hooks";
import {Link} from "react-router-dom";
import {routes} from "../../../../router/routes";
import {isCurrentRoute} from "../../../../lib/helpers";

const gridSize = "Размер сетки";
const otherPages = "Другие страницы:";

const ControlPanel = () => {
	const { showControlPanel, gridCellSize } = useSelector(state => state.map);
	const dispatch = useDispatch();
	
	const [ cellSizeSliderValue, setCellSizeSliderValue ] = useState(gridCellSize);
	
	const debouncedCellSizeSliderValue = useDebounce(cellSizeSliderValue, 20);
	
	const onCellSizeSliderValueChange = useCallback((event, newValue) => {
		setCellSizeSliderValue(newValue);
	}, [ setCellSizeSliderValue ])
	
	useEffect(() => {
		if (debouncedCellSizeSliderValue !== gridCellSize) {
			dispatch(changeGridCellSize(debouncedCellSizeSliderValue));
		}
	}, [debouncedCellSizeSliderValue]);
	
	const rootClassName = classNames("map-page__control-panel", {
		"map-page__control-panel--hidden": !showControlPanel
	});
	
	return (
		<div className={rootClassName}>
			<div className="map-page__control-panel-content">
				<div className="map-page__conrol-panel-field">
					<span>{ gridSize }</span>
					<Slider
						value={cellSizeSliderValue}
						onChange={onCellSizeSliderValueChange}
						min={25}
						max={200}
						valueLabelDisplay="auto"
					/>
				</div>
				
				<Divider />
				
				<div className="map-page__conrol-panel-field">
					<span className="map-page__conrol-panel-link-title">
						{ otherPages }
					</span>
				</div>
				
				{ routes.filter(route => !isCurrentRoute(route.url)).map(route => (
					<div className="map-page__conrol-panel-link" key={route.url}>
						<Link to={route.url}>
							{ route.title }
						</Link>
					</div>
				))}
			</div>
			
			<div
				className="map-page__control-panel-button"
				onClick={() => dispatch(toggleControlPanel())}
			>
				{ showControlPanel ? <CloseIcon /> : <SettingsIcon /> }
 			</div>
		</div>
	);
};

export default ControlPanel;
