import React, {useEffect, useMemo, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import MapGridCell from "./mapGridCell";
import {Box} from "@mui/material";
import {createGraph} from "../../../../store/map/mapSlice";
import useGridStyles from "../../hooks/useGridStyles";

const MapGrid = () => {
	const {
		gridContainerInlineStyle,
		gridInlineStyle,
		gridCellWrapperInlineStyle
	} = useGridStyles();
	const { fullGraph } = useSelector(state => state.map);
	const [ containerElement, setContainerElement ] = useState(null);
	const dispatch = useDispatch();
	
	useEffect(() => {
		if (containerElement && !fullGraph) {
			const { width, height } = containerElement.getBoundingClientRect();
			dispatch(createGraph(width, height));
		}
	}, [ containerElement ]);
	
	return (
		<div
			className="map-page__grid-container"
			ref={setContainerElement}
			style={gridContainerInlineStyle}
		>
			<Box sx={gridInlineStyle}>
				{ fullGraph && fullGraph.getVerticesList().map((vertex) => (
					<div
						className="map-page__grid-cell-wrapper"
						style={gridCellWrapperInlineStyle}
						key={vertex}
					>
						<MapGridCell
							vertex={vertex}
						/>
					</div>
				))}
			</Box>
		</div>
	);
};

export default MapGrid;
