import React, {useCallback} from "react";
import classNames from "classnames";
import {useDispatch, useSelector} from "react-redux";
import {includeExcludedVertex, setSelectedVertex} from "../../../../store/map/mapSlice";
import {getRangeByDistance} from "../../helpers/getRangeByDistance";

const MapGridCell = ({ vertex }) => {
	const {
		excludedVertices,
		selectedVertex,
		selectedVertexDistances,
		showRange
	} = useSelector(state => state.map);
	
	const dispatch = useDispatch();
	
	const rangeNumber = getRangeByDistance(selectedVertexDistances[vertex]);
	
	const rootClassName = classNames("map-page__grid-cell", {
		"map-page__grid-cell--selected": selectedVertex === vertex,
		[`map-page__grid-cell--range-${rangeNumber}`]: showRange && selectedVertex !== vertex,
		"map-page__grid-cell--excluded": excludedVertices.includes(vertex)
	});
	
	const onGridCellClick = useCallback(() => {
		if (excludedVertices.includes(vertex)) {
			dispatch(includeExcludedVertex(vertex));
			return;
		}
		
		dispatch(setSelectedVertex(vertex));
	}, [ dispatch, vertex, excludedVertices ]);
	
	return (
		<div
			className={rootClassName}
			onClick={onGridCellClick}
		/>
	);
};

export default MapGridCell;
