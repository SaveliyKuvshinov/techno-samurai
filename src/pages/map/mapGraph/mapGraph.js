const Graph = require("algorithms").DataStructures.Graph;
const GraphAlgorithms = require("algorithms").Graph;

function MapGraph(directed, cellsInRow, cellsInColumn) {
	Graph.call(this, directed);
	
	this.cellsInRow = cellsInRow;
	this.cellsInColumn = cellsInColumn;
}

MapGraph.prototype = Object.create(Graph.prototype);
MapGraph.prototype.constructor = Graph;

MapGraph.prototype.getVerticesList = function () {
	return Object.keys(this.adjList);
}

MapGraph.prototype.getDistanceToVertices = function (selectedVertex) {
	const result = GraphAlgorithms.SPFA(this, selectedVertex);
	return result.distance;
}

export default MapGraph;
