export const getRangeByDistance = (distance) => {
	if (distance < 3) {
		return distance;
	}
	
	if (distance < 6) {
		return 3;
	}
	
	if (distance < 10) {
		return 4;
	}
	
	if (distance < 15) {
		return 5;
	}
	
	return 6;
};