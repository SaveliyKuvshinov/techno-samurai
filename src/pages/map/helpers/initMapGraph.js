import MapGraph from "../mapGraph";

export const initMapGraph = (cellsInRow, cellsInColumn) => {
	const graph = new MapGraph(false, cellsInRow, cellsInColumn);
	
	let vertex = 0;
	
	for (let row = 0; row < cellsInColumn; row++) {
		const cellsInCurrentRow = cellsInRow - row % 2;
		const cellsInPreviousRow = row !== 0
			? cellsInRow - (row - 1) % 2
			: 0;
		
		for (let column = 0; column < cellsInCurrentRow; column++) {
			graph.addVertex(vertex);
			
			if (column !== 0) {
				graph.addEdge(vertex, vertex - 1);
			}
			
			if (cellsInPreviousRow > 0) {
				if (cellsInCurrentRow === cellsInRow) {
					if (column === 0) {
						graph.addEdge(vertex, vertex - cellsInPreviousRow);
					}
					else if (column + 1 === cellsInCurrentRow) {
						graph.addEdge(vertex, vertex - cellsInPreviousRow - 1);
					}
					else {
						graph.addEdge(vertex, vertex - cellsInPreviousRow - 1);
						graph.addEdge(vertex, vertex - cellsInPreviousRow);
					}
				}
				else {
					graph.addEdge(vertex, vertex - cellsInPreviousRow);
					graph.addEdge(vertex, vertex - cellsInPreviousRow + 1);
				}
			}
			
			vertex++;
		}
	}
	
	return graph;
};
