export const getCellsCountByDimensions = (width, height, cellSize) => {
	const cellHeight = cellSize * 2 / Math.sqrt(3);
	const rowHeight = (3 * cellSize) / (2 * Math.sqrt(3));
	
	return {
		cellsInRow: Math.floor(width / cellSize),
		cellsInColumn: Math.floor((height - cellHeight) / rowHeight) + 1,
	}
}