import {useSelector} from "react-redux";
import {useMemo} from "react";

const useGridStyles = () => {
	const { gridCellSize, fullGraph } = useSelector(state => state.map);
	
	const gridCellHeight = gridCellSize * 2 / Math.sqrt(3);
	const gridCellMarginBottom = gridCellSize / (2 * Math.sqrt(3));
	const doubleRowHeight = 2 * (gridCellHeight - gridCellMarginBottom);
	
	const gridCellWrapperInlineStyle = {
		width: `${gridCellSize}px`,
		height: `${gridCellHeight}px`,
		marginBottom: `-${gridCellMarginBottom}px`
	};
	
	const gridInlineStyle = {
		width: "100%",
		height: "100%",
		'&::before': {
			content: '""',
			width: `${gridCellSize / 2}px`,
			shapeOutside: `repeating-linear-gradient(transparent 0 calc(${doubleRowHeight - 1}px - 3px), #fff 0 ${doubleRowHeight - 1}px)`,
			float: "left",
			height: "100%"
		}
	};
	
	const gridContainerInlineStyle = useMemo(() => ({
		width: fullGraph ? `${fullGraph.cellsInRow * gridCellSize}px` : "100%",
		height: fullGraph ? `${fullGraph.cellsInColumn * doubleRowHeight / 2 + gridCellMarginBottom}px` : "100%"
	}), [ fullGraph, gridCellSize ]);
	
	return {
		gridContainerInlineStyle,
		gridInlineStyle,
		gridCellWrapperInlineStyle,
	}
};

export default useGridStyles;
