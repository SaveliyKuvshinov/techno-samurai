import {useDispatch, useSelector} from "react-redux";
import {useCallback} from "react";
import {excludeSelectedVertex, setShowRange} from "../../../store/map/mapSlice";


const useKeyActions = (props) => {
	const { showRange } = useSelector(state => state.map);
	const dispatch = useDispatch();
	
	const showRangeKeyAction = useCallback(() => {
			dispatch(setShowRange(true))
	}, [ dispatch ]);
	
	const hideRangeKeyAction = useCallback(() => {
		dispatch(setShowRange(false))
	}, [ dispatch ]);
	
	const excludeSelectedVertexKeyAction = useCallback(() => {
		dispatch(excludeSelectedVertex())
	}, [ dispatch ]);
	
	const keyDownCallbackWrapper = useCallback((event) => {
		const keyActionsMap = {
			KeyR: showRangeKeyAction
		}
		
		keyActionsMap[event.code]?.();
	}, [ dispatch, showRangeKeyAction ]);
	
	const keyUpCallbackWrapper = useCallback((event) => {
		const keyActionsMap = {
			KeyR: hideRangeKeyAction
		}
		
		keyActionsMap[event.code]?.();
	}, [ dispatch, hideRangeKeyAction ]);
	
	const keyPressCallbackWrapper = useCallback((event) => {
		const keyActionsMap = {
			KeyD: excludeSelectedVertexKeyAction
		}
		
		keyActionsMap[event.code]?.();
	})
	
	return {
		keyDownCallbackWrapper,
		keyUpCallbackWrapper,
		keyPressCallbackWrapper
	}
};

export default useKeyActions;
