import React from "react";
import {Accordion, AccordionDetails, AccordionSummary} from "@mui/material";
import {ExpandMore} from "@mui/icons-material";

const SectionItem = ({ id, title, description, summary }) => {
	return (
		<Accordion>
			<AccordionSummary
				expandIcon={<ExpandMore />}
				id={id}
			>
				<span className="knowledge-base-page__section-item-title">
					{ title }
				</span>
			</AccordionSummary>
			<AccordionDetails>
				<span className="knowledge-base-page__section-item-description">
					{ description }
				</span>
				
				{ summary &&
					<div className="knowledge-base-page__section-item-summary">
						{ summary }
					</div>
				}
			</AccordionDetails>
		</Accordion>
	)
};

export default SectionItem;
