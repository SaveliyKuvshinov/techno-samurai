import React, {useEffect, useState} from "react";
import {TextField} from "@mui/material";
import {useDebounce} from "../../../../lib/hooks";
import {filterDataBySearchValue} from "../../../../store/knowledgeBase/knowledgeBaseSlice";
import {useDispatch} from "react-redux";

const searchFieldLabel = "Искать мудрость";

const SearchField = () => {
	const [ searchValue, setSearchValue ] = useState("");
	const dispatch = useDispatch();
	
	const onSearchValueChange = (event) => {
		setSearchValue(event.target.value);
	}
	
	const debouncedSearchValue = useDebounce(searchValue, 200);
	
	useEffect(() => {
		dispatch(filterDataBySearchValue(debouncedSearchValue));
	}, [ debouncedSearchValue ]);
	
	return (
		<div className="knowledge-base-page__search-field-container">
			<TextField
				variant="outlined"
				label={searchFieldLabel}
				value={searchValue}
				onChange={onSearchValueChange}
			/>
		</div>
	)
};

export default SearchField;
