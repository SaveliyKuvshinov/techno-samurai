import React from "react";
import SectionItem from "../sectionItem";
import {useSelector} from "react-redux";

const filteredDataByTypeSelector = (type) => (state) => {
	const filteredDataByType = {
		"conditions": state.knowledgeBase.filteredConditions,
		"terrainProperties": state.knowledgeBase.filteredTerrainProperties,
	}
	
	return filteredDataByType[type];
}

const Section = ({ title, type }) => {
	const filteredData = useSelector(filteredDataByTypeSelector(type));
	
	if (!filteredData || filteredData.length === 0) {
		return null;
	}
	
	return (
		<div className="knowledge-base-page__section">
			<div className="knowledge-base-page__section-title">
				{ `${title}:` }
			</div>
			
			<div className="knowledge-base-page__section-content">
				{ filteredData.map(item => (
					<SectionItem
						id={item.title}
						title={item.title}
						description={item.description}
						summary={item.removal}
					/>
				))}
			</div>
		</div>
	)
};

export default Section;
