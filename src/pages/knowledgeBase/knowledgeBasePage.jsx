import React from "react";
import {sections} from "./constants";
import SearchField from "./components/searchField";
import Section from "./components/section";

const pageTitle = "Источник мудрости";

const KnowledgeBasePage = () => {
	return (
		<div className="knowledge-base-page">
			<div className="knowledge-base-page__content">
				<div className="knowledge-base-page__title">
					{ pageTitle }
				</div>
				
				<SearchField />
				
				{ sections.map(section => (
					<Section
						key={section.type}
						title={section.title}
						type={section.type}
					/>
				))}
			</div>
		</div>
	)
};

export default KnowledgeBasePage;
