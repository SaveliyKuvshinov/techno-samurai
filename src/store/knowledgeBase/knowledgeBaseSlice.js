import { createSlice } from "@reduxjs/toolkit";
import {conditions, terrainProperties} from "../../pages/knowledgeBase/constants";
import {isEmptyString} from "../../lib/helpers";

const initialState = {
	filteredConditions: conditions,
	filteredTerrainProperties: terrainProperties,
};

const knowledgeBaseSlice = createSlice({
	name: "app",
	initialState,
	reducers: {
		filterDataBySearchValue(state, action) {
			if (isEmptyString(action.payload)) {
				state.filteredConditions = conditions;
				state.filteredTerrainProperties = terrainProperties;
				
				return;
			}
			
			const getFilteredSection = (section) =>
				section.filter(sectionItem => sectionItem.title.toLowerCase().includes(action.payload.toLowerCase()));
			
			state.filteredConditions = getFilteredSection(conditions);
			state.filteredTerrainProperties = getFilteredSection(terrainProperties);
		}
	}
});

export const { filterDataBySearchValue } = knowledgeBaseSlice.actions;

export default knowledgeBaseSlice.reducer;
