import { createSlice } from "@reduxjs/toolkit";
import defaultBackground from "../../assets/background/bamboo-forest.gif";

const initialState = {
	showSidebar: false,
	isFullscreen: false,
	backgroundImage: defaultBackground
};

const appSlice = createSlice({
	name: "app",
	initialState,
	reducers: {
		toggleSidebar(state, action) {
			state.showSidebar = !state.showSidebar;
		},
		toggleFullscreen(state, action) {
			state.isFullscreen = !state.isFullscreen;
		}
	}
});

export const { toggleSidebar, toggleFullscreen } = appSlice.actions;

export default appSlice.reducer;
