import { createSlice } from "@reduxjs/toolkit";
import {initMapGraph} from "../../pages/map/helpers/initMapGraph";
import {getCellsCountByDimensions} from "../../pages/map/helpers/getCellsCountByDimensions";

const initialState = {
	gridCellSize: 52,
	selectedVertex: null,
	selectedVertexDistances: {},
	excludedVertices: [],
	showRange: false,
	fullGraph: null,
	actualGraph: null,
	showControlPanel: false,
	containerWidth: 0,
	containerHeight: 0,
};

const mapSlice = createSlice({
	name: "map",
	initialState,
	reducers: {
		changeGridCellSize(state, action) {
			const gridCellSize = action.payload;
			
			const { cellsInRow, cellsInColumn } = getCellsCountByDimensions(
				state.containerWidth,
				state.containerHeight,
				gridCellSize
			);
			
			state.gridCellSize = gridCellSize;
			state.fullGraph = initMapGraph(cellsInRow, cellsInColumn);
			state.actualGraph = initMapGraph(cellsInRow, cellsInColumn);
			state.selectedVertex = null;
			state.showRange = null;
		},
		setSelectedVertex(state, action) {
			if (state.selectedVertex === action.payload) {
				state.selectedVertex = null;
				return;
			}
			
			state.selectedVertex = action.payload;
			state.selectedVertexDistances = state.actualGraph.getDistanceToVertices(action.payload);
		},
		setShowRange(state, action) {
			if (state.showRange === action.payload || !state.selectedVertex && action.payload === true) {
				return;
			}
			
			state.showRange = action.payload;
		},
		createGraph: {
			prepare(width, height) {
				return {
					payload: { width, height }
				}
			},
			reducer(state, action) {
				const { width, height } = action.payload;
				const { cellsInRow, cellsInColumn } = getCellsCountByDimensions(width, height, state.gridCellSize);
				
				state.containerWidth = width;
				state.containerHeight = height;
				
				state.fullGraph = initMapGraph(cellsInRow, cellsInColumn);
				state.actualGraph = initMapGraph(cellsInRow, cellsInColumn);
			}
		},
		toggleControlPanel(state, action) {
			state.showControlPanel = !state.showControlPanel;
		},
		excludeSelectedVertex(state, action) {
			if (state.selectedVertex === null) {
				return;
			}
			
			const vertex = state.selectedVertex;
			const neighbours = state.actualGraph.adjList[vertex];
			
			for (const neighbour in neighbours) {
				delete state.actualGraph.adjList[neighbour][vertex];
			}
			
			delete state.actualGraph.adjList[vertex];
			
			state.selectedVertex = null;
			state.excludedVertices.push(vertex);
		},
		includeExcludedVertex(state, action) {
			const vertex = action.payload;
			
			state.excludedVertices = state.excludedVertices.filter(excludedVertex => excludedVertex !== vertex);
			
			state.actualGraph.adjList[vertex] = Object.keys(state.fullGraph.adjList[vertex]).reduce(
				(actualNeighbours, currentNeighbour) => {
					if (state.excludedVertices.includes(currentNeighbour)) {
						return actualNeighbours;
					}
					
					return { ...actualNeighbours, [currentNeighbour]: 1 };
				},
				{}
			);
			
			Object.keys(state.actualGraph.adjList[vertex]).forEach(neighbour => {
				state.actualGraph.adjList[neighbour][vertex] = 1;
			});
			
			if (state.selectedVertex) {
				state.selectedVertexDistances = state.actualGraph.getDistanceToVertices(state.selectedVertex);
			}
		}
	},
});

export const {
	changeGridCellSize,
	setSelectedVertex,
	createGraph,
	setShowRange,
	toggleControlPanel,
	excludeSelectedVertex,
	includeExcludedVertex,
} = mapSlice.actions;

export default mapSlice.reducer;
