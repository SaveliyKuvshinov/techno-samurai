import { configureStore } from "@reduxjs/toolkit";

import appReducer from "./app/appSlice";
import mapReducer from "./map/mapSlice";
import knowledgeBaseReducer from "./knowledgeBase/knowledgeBaseSlice";

const store = configureStore({
	reducer: {
		app: appReducer,
		map: mapReducer,
		knowledgeBase: knowledgeBaseReducer,
	},
	middleware: getDefaultMiddleware => getDefaultMiddleware({ serializableCheck: false })
})

export default store;