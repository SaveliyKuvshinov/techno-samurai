import HomePage from "../pages/home";
import {Map, MenuBook, Quiz} from "@mui/icons-material";
import MapPage from "../pages/map";
import KnowledgeBasePage from "../pages/knowledgeBase";

export const routes = [
	{
		url: "/",
		title: "Домашняя",
		component: <HomePage />,
		hideInNavigation: true
	},
	{
		url: "/map",
		title: "Карта",
		component: <MapPage />,
		icon: Map
	},
	{
		url: "/knowledge-base",
		title: "Справочник",
		component: <KnowledgeBasePage />,
		icon: Quiz
	}
];