import {routes} from "../../router/routes";
import {isCurrentRoute} from "./isCurrentRoute";

export const getCurrentRoute = () => {
	return routes.find(route => isCurrentRoute(route.url));
}
