import {isCurrentRoute} from "./isCurrentRoute";
import {isEmptyString} from "./isEmptyString";

export {
	isCurrentRoute,
	isEmptyString,
}