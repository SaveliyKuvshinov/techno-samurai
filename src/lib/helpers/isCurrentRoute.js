export const isCurrentRoute = (url) => {
	return url === window.location.pathname;
};