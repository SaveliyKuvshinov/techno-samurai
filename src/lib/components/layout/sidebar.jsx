import React from "react";
import {useSelector} from "react-redux";
import classNames from "classnames";
import {routes} from "../../../router/routes";
import {Link} from "react-router-dom";

const Sidebar = () => {
	const { showSidebar } = useSelector(state => state.app);
	
	const rootClassName = classNames("sidebar", {
		"sidebar--hidden": !showSidebar
	});
	
	return (
		<div className={rootClassName}>
			{ routes.map(route => (
				<div className="sidebar__item">
					<Link to={route.url}>{ route.title }</Link>
				</div>
			))}
		</div>
	);
};

export default Sidebar;
