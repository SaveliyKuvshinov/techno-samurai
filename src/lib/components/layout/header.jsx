import React, {useCallback} from "react";
import {AppBar, IconButton} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import {toggleSidebar} from "../../../store/app/appSlice";
import {useDispatch} from "react-redux";

const Header = () => {
	const dispatch = useDispatch();
	
	const onSidebarClick = useCallback(() => {
		dispatch(toggleSidebar());
	}, [ dispatch ]);
	
	return (
		<div className="header">
			<IconButton
				size="large"
				edge="start"
				color="inherit"
				aria-label="menu"
				onClick={onSidebarClick}
			>
				<MenuIcon />
			</IconButton>
		</div>
	)
};

export default Header;
