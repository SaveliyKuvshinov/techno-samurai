import React from "react";
import {useSelector} from "react-redux";

const Layout = ({ children }) => {
	const { backgroundImage } = useSelector(state => state.app);
	
	return (
		<div className="layout">
			<img className="layout__background-image" src={backgroundImage} alt="" />
			
			{ children }
		</div>
	)
};

export default Layout;
